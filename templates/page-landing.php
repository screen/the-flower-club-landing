<?php
/**
* Template Name: Template - Landing Page
*
* @package flowerclub
* @subpackage flowerclub-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header('landing'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php /* SLIDER SECTION */ ?>
        <?php $activate = get_post_meta( get_the_ID(), 'tfc_landing_slider_activate', true ); ?>
        <?php if ($activate == 'on' ) { ?>
        <section class="the-slider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row">
                <div class="slider-container owl-carousel owl-theme col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <?php $slider_items = get_post_meta(get_the_ID(), 'tfc_landing_slider_group', true); ?>
                    <?php if (!empty($slider_items)) { ?>
                    <?php foreach ($slider_items as $slider_item) { ?>
                    <?php if ($slider_item['invert'] == 'on') { $class = 'inverted'; } else { $class = ''; } ?>
                    <div class="slider-item <?php echo $class; ?> col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $slider_item['bg']; ?>);">
                        <div class="row">
                            <div class="slider-image col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-xl-none d-lg-none d-md-flex d-sm-flex d-flex">
                                <img src="<?php echo $slider_item['mobile_bg']; ?>" alt="Image" class="img-fluid" />
                            </div>
                            <?php if ($slider_item['invert'] == 'on') { ?>
                            <div class="slider-text inverted col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 offset-xl-7 offset-lg-7">
                                <?php echo apply_filters('the_content', $slider_item['description']); ?>
                                <div class="slider-text-button">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/icon-delivery.png" alt="Icon" class="img-fluid"> <?php _e('Free Shipping!', 'flowerclub'); ?>
                                </div>
                                <div class="slider-buttons col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <?php if ($slider_item['special_banner'] === 'on') { ?>
                                    <?php if ($slider_item['button_1_url'] != '') { ?>
                                    <div data-url="<?php echo $slider_item['button_1_url']; ?>" class="btn btn-md btn-slider btn-share"><span><?php echo $slider_item['button_1']; ?></span>
                                        <?php $lang = get_bloginfo("language");  ?>
                                        <?php if ($lang == 'en-US') { ?>
                                        <?php $message = urlencode('🌹Hi there, sign up to receive a free bouquet!🎉 http://promotion.theflowerclub.net #FlowersBySubscription #FreeBouquet'); ?>
                                        <?php } else { ?>
                                        <?php $message = urlencode('🌹¡Hola! ¡Suscríbete para recibir un bouquet gratuito!🎉 http://promotion.theflowerclub.net #FlowersBySubscription #FreeBouquet'); ?>
                                        <?php } ?>
                                        <small class="sharer-buttons">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=http://promotion.theflowerclub.net" target="_blank" title="<?php _e('Share on Facebook', 'flowerclub'); ?>"><i class="fa fa-facebook"></i></a>
                                            <a href="https://twitter.com/intent/tweet?text=<?php echo $message; ?>" target="_blank" title="<?php _e('Share on Twitter', 'flowerclub')?>"><i class="fa fa-twitter"></i></a>
                                            <a href=" https://wa.me/?text=<?php echo $message; ?>" data-action="share/whatsapp/share" target="_blank"><i class="fa fa-whatsapp"></i></a>
                                            <a href="mailto:youremail@gmail.com?subject=The Flower Club and Harmony&body=<?php echo $message; ?>" target="_blank"><i class="fa fa-envelope-o"></i></a>
                                        </small>
                                    </div>
                                    <?php } ?>
                                    <?php if ($slider_item['button_2_url'] != '') { ?>
                                    <a href="<?php echo $slider_item['button_2_url']; ?>" class="btn btn-md btn-slider btn-modal-slider" data-toggle="modal" data-target="#exampleModal"><?php echo $slider_item['button_2']; ?></a>
                                    <?php } ?>
                                    <?php } else { ?>

                                    <?php if ($slider_item['button_1_url'] != '') { ?>
                                    <a data-scroll href="<?php echo $slider_item['button_1_url']; ?>" class="btn btn-md btn-slider"><?php echo $slider_item['button_1']; ?></a>
                                    <?php } ?>
                                    <?php if ($slider_item['button_2_url'] != '') { ?>
                                    <a data-url="<?php echo $slider_item['button_2_url']; ?>" class="btn btn-md btn-slider btn-video"><?php echo $slider_item['button_2']; ?></a>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="slider-text col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                                <?php echo apply_filters('the_content', $slider_item['description']); ?>
                                <div class="slider-text-button">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/icon-delivery.png" alt="Icon" class="img-fluid"> <?php _e('Free Shipping!', 'flowerclub'); ?>
                                </div>
                                <div class="slider-buttons col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <?php if ($slider_item['special_banner'] === 'on') { ?>

                                    <?php if ($slider_item['button_1_url'] != '') { ?>
                                    <div data-url="<?php echo $slider_item['button_1_url']; ?>" class="btn btn-md btn-slider btn-share"><span><?php echo $slider_item['button_1']; ?></span>
                                        <?php $lang = get_bloginfo("language");  ?>
                                        <?php if ($lang == 'en-US') { ?>
                                        <?php $message = urlencode('🌹Hi there, sign up to receive a free bouquet!🎉 http://promotion.theflowerclub.net #FlowersBySubscription #FreeBouquet'); ?>
                                        <?php } else { ?>
                                        <?php $message = urlencode('🌹¡Hola! ¡Suscríbete para recibir un bouquet gratuito!🎉 http://promotion.theflowerclub.net #FlowersBySubscription #FreeBouquet'); ?>
                                        <?php } ?>
                                        <small class="sharer-buttons">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=http://promotion.theflowerclub.net" target="_blank" title="<?php _e('Share on Facebook', 'flowerclub'); ?>"><i class="fa fa-facebook"></i></a>
                                            <a href="https://twitter.com/intent/tweet?text=<?php echo $message; ?>" target="_blank" title="<?php _e('Share on Twitter', 'flowerclub')?>"><i class="fa fa-twitter"></i></a>
                                            <a href=" https://wa.me/?text=<?php echo $message; ?>" data-action="share/whatsapp/share" target="_blank"><i class="fa fa-whatsapp"></i></a>
                                            <a href="mailto:youremail@gmail.com?subject=The Flower Club and Harmony&body=<?php echo $message; ?>" target="_blank"><i class="fa fa-envelope-o"></i></a>
                                        </small>
                                    </div>
                                    <?php } ?>
                                    <?php if ($slider_item['button_2_url'] != '') { ?>
                                    <a href="<?php echo $slider_item['button_2_url']; ?>" class="btn btn-md btn-slider btn-modal-slider" data-toggle="modal" data-target="#exampleModal"><?php echo $slider_item['button_2']; ?></a>
                                    <?php } ?>
                                    <?php } else { ?>

                                    <?php if ($slider_item['button_1_url'] != '') { ?>
                                    <a data-scroll href="<?php echo $slider_item['button_1_url']; ?>" class="btn btn-md btn-slider"><?php echo $slider_item['button_1']; ?></a>
                                    <?php } ?>
                                    <?php if ($slider_item['button_2_url'] != '') { ?>
                                    <a data-url="<?php echo $slider_item['button_2_url']; ?>" class="btn btn-md btn-slider btn-video"><?php echo $slider_item['button_2']; ?></a>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php } ?>
        <?php /* HOW IT WORKS SECTION */ ?>
        <?php if ( get_post_meta( get_the_ID(), 'tfc_landing_works_activate', true ) ) : ?>
        <?php $works_bg_id = get_post_meta(get_the_ID(), 'tfc_works_background_id', true); ?>
        <?php $works_bg = wp_get_attachment_image_src($works_bg_id, 'full'); ?>
        <section id="works" class="the-works col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $works_bg[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="works-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 class="section-title"><?php echo get_post_meta(get_the_ID(), 'tfc_works_title', true); ?></h2>
                        <div class="works-description">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tfc_works_description', true)); ?>
                        </div>
                    </div>
                    <div class="works-items-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            <?php $works_items = get_post_meta(get_the_ID(), 'tfc_landing_works_group', true); ?>
                            <?php if (!empty($works_items)) { ?>
                            <?php foreach ($works_items as $works_item) {?>
                            <div class="works-item col-xl col-lg col-md col-sm-6 col-12">
                                <img src="<?php echo $works_item['icon']; ?>" alt="Icon" class="img-fluid" />
                                <?php echo apply_filters('the_content', $works_item['description']); ?>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="works-buttons-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $lang = get_bloginfo("language"); ?>
                        <?php $works_items = get_post_meta(get_the_ID(), 'tfc_landing_works_btn_group', true); $i = 1; ?>
                        <?php if (!empty($works_items)) { ?>
                        <?php foreach ($works_items as $works_item) { ?>
                        <a href="<?php echo apply_filters('the_content', $works_item['description']); ?>" title="<?php _e('Get it now!', 'flowerclub'); ?>">
                            <?php if ($lang != 'en-US') { $class = '-esp'; } else { $class = ''; } ?>
                            <span class="body-app-gallery-btn body-app-gallery-btn-<?php echo $i . $class; ?>"></span>
                        </a>
                        <?php $i++; } ?>
                        <?php } ?>
                    </div>
                    <?php if (get_post_meta(get_the_ID(), 'tfc_works_btn_url', true) != '') { ?>
                    <div class="works-cta-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'tfc_works_btn_url', true); ?>" class="btn btn-md btn-blog"><?php echo get_post_meta(get_the_ID(), 'tfc_works_btn_title', true); ?></a>
                        <p><?php echo get_post_meta(get_the_ID(), 'tfc_works_btn_description', true); ?></p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <?php /* PLAN SECTIONS */ ?>
        <?php if ( get_post_meta( get_the_ID(), 'tfc_landing_plans_activate', true ) ) : ?>
        <section class="the-plans col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="plans-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 class="section-title"><?php echo get_post_meta(get_the_ID(), 'tfc_plans_title', true); ?></h2>
                        <div class="plans-description">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tfc_plans_description', true)); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="plans-items-container col-xl-10 col-lg-11 col-md-12 col-sm-12 col-12">
                        <div class="row justify-content-center">
                            <?php $args = array('post_type' => 'plans', 'posts_per_page' => 3, 'order' => 'ASC', 'orderby' => 'date'); $i = 1; ?>
                            <?php $array_plans = new WP_Query($args); ?>
                            <?php if ($array_plans->have_posts()) : ?>
                            <?php while ($array_plans->have_posts()) : $array_plans->the_post(); ?>
                            <article class="plans-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="plans-item-content">
                                    <div class="plans-item-picture">
                                        <?php if ($i == 2) { ?>
                                        <div class="plans-item-best">
                                            <?php _e('Best Value!', 'flowerclub'); ?>
                                        </div>
                                        <?php } ?>
                                        <?php the_post_thumbnail('plan_img', array('class' => 'img-fluid')); ?>
                                    </div>
                                    <div class="plans-item-title">
                                        <h2><?php the_title(); ?></h2>
                                    </div>
                                    <div class="plans-item-features">
                                        <?php $features_group = get_post_meta(get_the_ID(), 'tfc_plans_featured_group', true); ?>
                                        <?php foreach ($features_group as $feature_item) { ?>
                                        <p><?php echo htmlspecialchars($feature_item['features']); ?></p>
                                        <?php } ?>
                                    </div>
                                    <div class="plans-item-price">
                                        <h4><?php echo get_post_meta(get_the_ID(), 'tfc_plans_price', true); ?></h4>
                                        <span><?php echo get_post_meta(get_the_ID(), 'tfc_plans_price_message', true); ?></span>
                                    </div>
                                    <div class="plans-item-button">
                                        <?php $img_id = get_post_meta(get_the_ID(), 'tfc_plans_icon_id', true); ?>
                                        <?php $plang_img = wp_get_attachment_image_src($img_id, 'full'); ?>
                                        <a class="btn btn-md btn-plans">
                                            <img src="<?php echo $plang_img[0]; ?>" alt="" class="img-fluid" /> <?php _e('Free Shipping!', 'flowerclub'); ?>
                                        </a>
                                    </div>
                                </div>
                            </article>
                            <?php $i++; endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                    <?php if (get_post_meta(get_the_ID(), 'tfc_plans_btn_url', true) != '') { ?>
                    <div class="plans-cta-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'tfc_plans_btn_url', true); ?>" class="btn btn-md btn-blog"><?php echo get_post_meta(get_the_ID(), 'tfc_plans_btn_title', true); ?></a>
                        <p><?php echo get_post_meta(get_the_ID(), 'tfc_plans_btn_description', true); ?></p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <?php /* CATALOG SECTIONS */ ?>
        <?php if ( get_post_meta( get_the_ID(), 'tfc_landing_catalog_activate', true ) ) : ?>
        <section class="the-catalog col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="catalog-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 class="section-title"><?php echo get_post_meta(get_the_ID(), 'tfc_catalog_title', true); ?></h2>
                        <div class="catalog-description">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tfc_catalog_description', true)); ?>
                        </div>
                    </div>
                    <?php $ordered_terms = get_post_meta(get_the_ID(), 'tfc_catalog_category_order', true); ?>
                    <div class="catalog-items-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <ul class="nav nav-tabs justify-content-center d-xl-flex d-lg-flex d-md-none d-sm-none d-none" id="myTab" role="tablist">
                            <?php if (empty($ordered_terms)) { ?>
                            <?php $terms = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false, 'parent' => 0 ) ); $i = 1; ?>
                            <?php if ( !empty($terms) ) : ?>
                            <?php foreach( $terms as $category ) { ?>
                            <?php if ($i == 1) { $class = 'active'; } else { $class = ''; } ?>
                            <li class="nav-item">
                                <a class="nav-link <?php echo $class; ?>" id="<?php echo $category->slug; ?>-tab" data-toggle="tab" href="#<?php echo $category->slug; ?>" role="tab" aria-controls="<?php echo $category->slug; ?>" aria-selected="true"><?php echo $category->name; ?></a>
                            </li>
                            <?php $i++; } ?>
                            <?php endif; ?>
                            <?php } else { ?>
                            <?php foreach ($ordered_terms as $item) { ?>
                            <?php $term = get_term_by('id', $item, 'product_cat'); ?>
                            <?php if ($i == 1) { $class = 'active'; } else { $class = ''; } ?>
                            <li class="nav-item">
                                <a class="nav-link <?php echo $class; ?>" id="<?php echo $term->slug; ?>-tab" data-toggle="tab" href="#<?php echo $term->slug; ?>" role="tab" aria-controls="<?php echo $term->slug; ?>" aria-selected="true"><?php echo $term->name; ?></a>
                            </li>
                            <?php $i++; } ?>
                            <?php } ?>
                        </ul>
                        <select name="tab_changer" id="tabMobile" class="form-control form-select d-xl-none d-lg-none d-md-block d-sm-block d-block">
                            <?php if (empty($ordered_terms)) { ?>
                            <?php $terms = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false, 'parent' => 0 ) ); $i = 1; ?>
                            <?php if ( !empty($terms) ) : ?>
                            <?php foreach( $terms as $category ) { ?>
                            <?php if ($i == 1) { $class = 'selected'; } else { $class = ''; } ?>
                            <option data-href="#<?php echo $category->slug; ?>" id="<?php echo $category->slug; ?>-option" <?php echo $class; ?>><?php echo $category->name; ?></option>
                            <?php $i++; } ?>
                            <?php endif; ?>
                            <?php } else { ?>
                            <?php foreach ($ordered_terms as $item) { ?>
                            <?php $term = get_term_by('id', $item, 'product_cat'); ?>
                            <?php if ($i == 1) { $class = 'selected'; } else { $class = ''; } ?>
                            <option data-href="#<?php echo $term->slug; ?>" id="<?php echo $term->slug; ?>-option" <?php echo $class; ?>><?php echo $term->name; ?></option>
                            <?php $i++; } ?>
                            <?php } ?>
                        </select>
                        <div class="tab-content" id="myTabContent">
                            <?php if (empty($ordered_terms)) { ?>
                            <?php $terms = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false, 'parent' => 0 ) ); $i = 1; ?>
                            <?php if ( !empty($terms) ) : ?>
                            <?php foreach( $terms as $category ) { ?>
                            <?php $cat_element = $category->slug; ?>
                            <?php if ($i == 1) { $class = 'show active'; } else { $class = ''; } ?>
                            <div class="tab-pane fade <?php echo $class; ?>" id="<?php echo $category->slug; ?>" role="tabpanel" aria-labelledby="<?php echo $category->slug; ?>-tab">
                                <?php if ($i != 1) { echo '<div class="loader"><div class="lds-dual-ring"></div></div>'; } ?>
                                <?php $array_products = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'title', 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $category->slug )))); ?>
                                <?php if ($array_products->have_posts()) : ?>
                                <div class="product-internal-slider owl-carousel owl-theme">
                                    <?php while ($array_products->have_posts()) : $array_products->the_post(); ?>
                                    <div id="<?php echo get_the_ID(); ?>" class="product-item" data-fancybox="products" data-src="#hidden-content<?php echo get_the_ID(); ?>" data-toolbar="true" href="javascript:;" href="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'avatar'); ?>" rel="products">
                                        <?php the_post_thumbnail('product_img', array('class' => 'img-fluid')); ?>
                                        <h3><?php the_title(); ?></h3>

                                        <div style="display: none;" id="hidden-content<?php echo get_the_ID(); ?>">
                                            <div class="container-fluid p-0">
                                                <div class="row no-gutters">
                                                    <div class="modal-product-image col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                        <?php $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                                                        <img src="<?php echo $image; ?>" alt="<?php echo get_the_title(); ?>" class="img-fluid img-product-modal" />
                                                    </div>
                                                    <div class="modal-product-text col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                        <div class="modal-product-text-content">
                                                            <?php $category = get_the_terms(get_the_ID(), 'product_cat'); ?>
                                                            <div class="category-container">
                                                                <?php foreach($category as $item) { ?>
                                                                <?php if( $item->parent == 0 ) { ?>
                                                                <span><?php echo $item->name; ?></span>
                                                                <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                            <h2><?php echo get_the_title(); ?></h2>
                                                            <div class="modal-product-text-container">
                                                                <?php the_content(); ?>
                                                            </div>
                                                            <div class="modal-btn-container">
                                                                <span><?php _e('Do you want it?', 'flowerclub'); ?></span>
                                                                <a href="https://chat.chatra.io/?hostId=ZTJZRAq4KwzWAtWGS#chatra" target="_blank" class="btn-red"><?php _e('Talk to an advisor!', 'flowerclub'); ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                            <?php $i++; } ?>
                            <?php endif; ?>
                            <?php } else { ?>
                            <?php $i = 1; ?>
                            <?php foreach ($ordered_terms as $item) { ?>
                            <?php $term = get_term_by('id', $item, 'product_cat'); ?>
                            <?php $cat_element = $term->slug; ?>
                            <?php if ($i == 1) { $class = 'show active'; } else { $class = ''; } ?>
                            <div class="tab-pane fade <?php echo $class; ?>" id="<?php echo $term->slug; ?>" role="tabpanel" aria-labelledby="<?php echo $term->slug; ?>-tab">
                                <?php if ($i != 1) { echo '<div class="loader"><div class="lds-dual-ring"></div></div>'; } ?>
                                <?php $array_products = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'title', 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $term->slug )))); ?>
                                <?php if ($array_products->have_posts()) : ?>
                                <div class="product-internal-slider owl-carousel owl-theme">
                                    <?php while ($array_products->have_posts()) : $array_products->the_post(); ?>
                                    <div id="<?php echo get_the_ID(); ?>" class="product-item" data-fancybox="products" data-src="#hidden-content<?php echo get_the_ID(); ?>" data-toolbar="true" href="javascript:;" href="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'avatar'); ?>" rel="products">
                                        <?php the_post_thumbnail('product_img', array('class' => 'img-fluid')); ?>
                                        <h3><?php the_title(); ?></h3>
                                        <div style="display: none;" id="hidden-content<?php echo get_the_ID(); ?>">
                                            <div class="container-fluid p-0">
                                                <div class="row no-gutters">
                                                    <div class="modal-product-image col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                        <?php $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                                                        <img src="<?php echo $image; ?>" alt="<?php echo get_the_title(); ?>" class="img-fluid img-product-modal" />
                                                    </div>
                                                    <div class="modal-product-text col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                        <div class="modal-product-text-content">
                                                            <?php $category = get_the_terms(get_the_ID(), 'product_cat'); ?>
                                                            <div class="category-container">
                                                                <?php foreach($category as $item) { ?>
                                                                <?php if( $item->parent == 0 ) { ?>
                                                                <span><?php echo $item->name; ?></span>
                                                                <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                            <h2><?php echo get_the_title(); ?></h2>
                                                            <div class="modal-product-text-container">
                                                                <?php the_content(); ?>
                                                            </div>
                                                            <div class="modal-btn-container">
                                                                <span><?php _e('Do you want it?', 'flowerclub'); ?></span>
                                                                <a href="https://chat.chatra.io/?hostId=ZTJZRAq4KwzWAtWGS#chatra" target="_blank" class="btn-red"><?php _e('Talk to an advisor!', 'flowerclub'); ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                            <?php $i++; } ?>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="w-100"></div>
                    <div class="catalog-button-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row">
                                <?php $embed = get_post_meta(get_the_ID(), 'tfc_catalog_btn_title', true); ?>
                                <?php if ($embed != '') { ?>
                                <div class="catalog-button-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <a class="btn btn-md btn-catalog-modal" data-toggle="modal" data-target="#catalogModal"><?php echo get_post_meta(get_the_ID(), 'tfc_catalog_btn_title', true); ?></a>
                                    <p class="catalog-modal-text"><?php echo get_post_meta(get_the_ID(), 'tfc_catalog_btn_description', true); ?></p>
                                    <!-- Modal -->
                                    <div class="modal modal-catalog fade" id="catalogModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
                                        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <div id="catalog-embed" class="embed-responsive embed-responsive-16by9">
                                                        <?php echo get_post_meta(get_the_ID(), 'tfc_catalog_embed', true); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <?php endif; ?>
        <?php /* BLOG SECTIONS */ ?>
        <?php if ( get_post_meta( get_the_ID(), 'tfc_landing_blog_activate', true ) ) : ?>
        <?php $blog_bg_id = get_post_meta(get_the_ID(), 'tfc_blog_background_id', true); ?>
        <?php $blog_bg = wp_get_attachment_image_src($blog_bg_id, 'full'); ?>
        <section class="the-blog col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $blog_bg[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="blog-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 class="section-title"><?php echo get_post_meta(get_the_ID(), 'tfc_blog_title', true); ?></h2>
                    </div>
                    <?php $blog_items = get_post_meta(get_the_ID(), 'tfc_landing_blog_group', true); ?>
                    <?php if (!empty($blog_items)) { ?>
                    <div class="blog-items-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="blog-items-slider owl-carousel owl-theme">
                            <?php foreach ($blog_items as $blog_item) {?>
                            <div class="blog-item">
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="blog-image col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                                            <img src="<?php echo $blog_item['image']; ?>" alt="Icon" class="img-fluid" />
                                        </div>
                                        <div class="blog-text col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                                            <h3><?php echo $blog_item['title']; ?></h3>
                                            <div class="blog-text-content">
                                                <?php echo apply_filters('the_content', $blog_item['description']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if (get_post_meta(get_the_ID(), 'tfc_blog_url', true) != '') { ?>
                    <div class="blog-buttons-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'tfc_blog_url', true); ?>" class="btn btn-md btn-blog"><?php echo get_post_meta(get_the_ID(), 'tfc_blog_btn_title', true); ?></a>
                        <p><?php echo get_post_meta(get_the_ID(), 'tfc_blog_btn_description', true); ?></p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php endif;  ?>
        <?php /* DESCANSO SECTIONS */ ?>
        <?php if ( get_post_meta( get_the_ID(), 'tfc_landing_hero_activate', true ) ) : ?>
        <?php $blog_bg_id = get_post_meta(get_the_ID(), 'tfc_hero_background_id', true); ?>
        <?php $blog_bg = wp_get_attachment_image_src($blog_bg_id, 'full'); ?>
        <section class="the-hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $blog_bg[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 class="section-title"><?php echo get_post_meta(get_the_ID(), 'tfc_hero_title', true); ?></h2>
                    </div>
                    <div class="hero-items-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row align-items-center justify-content-center">
                            <div class="hero-item col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tfc_hero_desc', true)); ?>
                            </div>
                            <div class="hero-item col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                                <?php $hero_internal_id = get_post_meta(get_the_ID(), 'tfc_hero_img_id', true); ?>
                                <?php $hero_internal = wp_get_attachment_image_src($hero_internal_id, 'full'); ?>
                                <img src="<?php echo $hero_internal[0]; ?>" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                    <?php if (get_post_meta(get_the_ID(), 'tfc_hero_btn_url', true) != '') { ?>
                    <div class="hero-cta-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'tfc_hero_btn_url', true); ?>" class="btn btn-md btn-blog"><?php echo get_post_meta(get_the_ID(), 'tfc_hero_btn_title', true); ?></a>
                        <p><?php echo get_post_meta(get_the_ID(), 'tfc_hero_btn_description', true); ?></p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-size modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="slider-form-container">
                    <h2><strong>Get a FREE Rose Bouquet</strong> and join a new and exclusive club in Miami Dade county!</h2>
                    <form class="form form-mailchimp-modal">
                        <div class="custom-form-control">
                            <div class="custom-form-control-icon">
                                <i class="fa fa-user-o"></i>
                            </div>
                            <input type="text" id="FNAME" name="FNAME" class="form-control" placeholder="<?php _e('First Name', 'flowerclub'); ?>" />
                            <small class="validate d-none"></small>
                        </div>
                        <div class="custom-form-control">
                            <div class="custom-form-control-icon">
                                <i class="fa fa-user-o"></i>
                            </div>
                            <input type="text" id="LNAME" name="LNAME" class="form-control" placeholder="<?php _e('Last Name', 'flowerclub'); ?>" />
                            <small class="validate d-none"></small>
                        </div>
                        <div class="custom-form-control">
                            <div class="custom-form-control-icon">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <input type="email" id="EMAIL" name="EMAIL" class="form-control" placeholder="<?php _e('Email Address', 'flowerclub'); ?>" />
                            <small class="validate d-none"></small>
                        </div>
                        <div class="custom-form-control">
                            <div class="custom-form-control-icon">
                                <i class="fa fa-mobile"></i>
                            </div>
                            <input type="text" id="PHONE" name="PHONE" class="form-control" placeholder="<?php _e('Phone Number', 'flowerclub'); ?>" />
                            <small class="validate d-none"></small>
                        </div>
                        <div class="custom-form-control">
                            <div class="custom-form-control-icon">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <input type="text" id="ZIPCODE" name="ZIPCODE" class="form-control" placeholder="<?php _e('Zip Code', 'flowerclub'); ?>" />
                            <small class="validate d-none"></small>
                        </div>
                        <?php $data_mailchimp = get_option('flowerclub_mailchimp_settings'); ?>
                        <input type="hidden" name="mailchimp_list" value="<?php echo $data_mailchimp['audience']; ?>" />
                        <input type="hidden" name="mailchimp_tag" value="<?php echo $data_mailchimp['tag']; ?>" />
                        <?php if (isset($_GET['utm_source'])) { $source_url = $_GET['utm_source']; } else { $source_url = 'Directo'; }?>
                        <input type="hidden" name="SOURCE" value="<?php echo $source_url; ?>" />
                        <?php if (isset($_GET['utm_medium'])) { $medium_url = $_GET['utm_medium']; } else { $medium_url = ''; }?>
                        <input type="hidden" name="MEDIUM" value="<?php echo $medium_url; ?>" />
                        <?php if (isset($_GET['utm_campaign'])) { $content_url = $_GET['utm_campaign']; } else { $content_url = ''; }?>
                        <input type="hidden" name="NAME" value="<?php echo $content_url; ?>" />
                        <button class="btn btn-md btn-submit" type="submit"><?php _e('Get your free bouquet!', 'flowerclub'); ?></button>
                        <div class="custom-mailchimp-connector-response"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer('landing'); ?>
