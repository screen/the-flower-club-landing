<?php
/**
* Template Name: Template - Thanks Page
*
* @package flowerclub
* @subpackage flowerclub-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header('empty'); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php $image_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
        <div class="thanks-page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $image_url; ?>);">
            <div class="container">
                <div class="row align-items-center row-thanks">
                    <div class="col-8 offset-xl-4">
                        <?php the_content(); ?>
                        <div class="social-container">
                            <a href="https://www.facebook.com/theflowerclubusa" title="<?php _e('Follow us on Facebook', 'flowerclub'); ?>"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/theflowerclubusa/" title="<?php _e('Follow us on Twitter', 'flowerclub'); ?>"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCdunB2loplTWRcAV_YNBrlg" title="<?php _e('Follow us on YouTube', 'flowerclub'); ?>"><i class="fa fa-youtube"></i></a>
                        </div>
                        <h3><?php _e('Meanwhile, follow us on our social media!', 'flowerclub'); ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer('landing'); ?>
