var passd = true;
jQuery(document).ready(function ($) {
    "use strict";
    jQuery("#sticker").sticky({
        topSpacing: 0,
        zIndex: 999
    });

    jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        var tab_target = jQuery(e.target).attr("href") // activated tab
        setTimeout(function () {
            jQuery(tab_target + ' .loader').remove();
        }, 600);

    });

    jQuery('#tabMobile').on('change', function (e) {
        var selected_tab = jQuery('option:selected', this).data('href');
        jQuery(selected_tab + '-tab').tab('show');
    });

    //    new ImageZoom(".img-product-modal", {
    //
    //        //  Maximum zoom level
    //        maxZoom: 2,
    //
    //        // 1 is the full axis, 0 nothing
    //        deadarea: 0.1,
    //
    //        // Duration of the animation of appearance/disappearance of the zoomed image
    //        appearDuration: 0.5,
    //
    //        // Description of the target area.
    //        // If null, the target area will be the current image provided.
    //        target: null,
    //
    //        // Explicit URL of the zoomed image.
    //        // If null, the source will be the attribute data-fullwidth-src of the image, or the attribute src of the image if the first is not defined
    //        imageUrl: null,
    //
    //        // Background color used behind images with transparence
    //        backgroundImageColor: null,
    //
    //        // If defined to false, source image deformations will be kept
    //        forceNaturalProportions: true
    //
    //    });
    //
    //    var instance = jQuery.fancybox.getInstance();
    //    console.log(instance);
    //
    //    jQuery.fancybox.open({
    //        afterShow: function (instance, current) {
    //            console.info(instance);
    //        }
    //    })

    jQuery('#catalog-embed iframe').addClass('embed-responsive-item');
    jQuery('.btn-video').on('click touchstart', function (e) {
        e.stopPropagation();
        var lastID = jQuery(this).data('url');
        jQuery.ajax({
            type: 'POST',
            url: admin_url.ajax_url,
            data: {
                action: 'flowerclub_view_video',
                info: lastID
            },
            beforeSend: function () {
                jQuery('.modal-body').html('');
                jQuery('#videoModal').modal('show');
            },
            success: function (response) {
                jQuery('.modal-body').html(response);
            },
            error: function (request, status, error) {
                console.log(error);
            }
        });
    });

    jQuery('.product-item').fancybox({
        helpers: {
            thumbs: {
                width: 40,
                height: 40

            }
        }
    });

    jQuery('#videoModal').on('hidden.bs.modal', function () {
        jQuery('.modal-body').html('');
    });

    jQuery('.slider-container').owlCarousel({
        items: 1,
        nav: false,
        dots: false,
        loop: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 3000
    });

    jQuery('.product-internal-slider').owlCarousel({
        margin: 30,
        items: 3,
        loop: false,
        rewind: true,
        nav: true,
        dots: true,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 1000,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            991: {
                items: 2
            },
            1024: {
                items: 3
            }
        }
    });

    jQuery('#lang-selector').on('change', function () {
        location.href = this.value;
    });

    jQuery('#lang-selector-mobile').on('change', function () {
        location.href = this.value;
    });



    jQuery('.blog-items-slider').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        dots: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 1000
    });

    var scroll = new SmoothScroll('a[data-scroll]', {

        // Selectors
        ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
        header: null, // Selector for fixed headers (must be a valid CSS selector)
        topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

        // Speed & Duration
        speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
        speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
        durationMax: null, // Integer. The maximum amount of time the scroll animation should take
        durationMin: null, // Integer. The minimum amount of time the scroll animation should take
        clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page

        // Easing
        easing: 'easeInOutCubic', // Easing pattern to use

        // History
        updateURL: true, // Update the URL on scroll
        popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

        // Custom Events
        emitEvents: true // Emit custom events

    });

    jQuery('#menu-opener').on('click', function (e) {
        "use strict";
        e.stopPropagation();
        jQuery(this).toggleClass('menu-bars-open');
        jQuery('#mobile-menu').toggleClass('header-mobile-hidden');
    });


    function validAlphaExp(text) {
        var alphaExp = /^[a-zA-Z]+$/;
        if (text.match(alphaExp)) {
            return true;
        } else {
            return false;
        }
    }


    /*
    var zipcodes_arr = ['33186', '33178', '33176', '33139', '33196', '33134', '33155', '33180', '33160', '33131', '33175', '33143', '33132', '33177', '33141', '33179', '33165', '33133', '33156', '33185', '33193', '33138', '33172', '33129', '33140', '33183', '33145', '33161', '33149', '33169', '33126', '33187', '33137', '33184', '33166', '33162', '33189', '33154', '33146', '33158', '33174', '33144', '33147', '33181', '33182', '33125', '33135', '33130', '33157', '33168', '33150', '33127', '33142', '33167', '33194', '33173', '33136', '33128', '33109', '33153', '33261', '33119', '33266', '33192'];

    jQuery('input[name=ZIPCODE]').autocomplete({
        lookup: zipcodes_arr,
        showNoSuggestionNotice: true,
        noSuggestionNotice: admin_url.not_found_zipcode
    });
*/
    function isValidEmailAddress(emailAddress) {
        'use strict';
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }

    function telephoneCheck(str) {
        var phoneNum = str.replace(/[^\d]/g, '');
        if (phoneNum.length > 6 && phoneNum.length < 11) {
            return true;
        }
    }

    jQuery('.btn-submit').on('click', function (e) {
        e.preventDefault();
        passd = true;

        if (jQuery('input[name=FNAME]').val() == '') {
            jQuery('input[name=FNAME]').next('small').removeClass('d-none');
            jQuery('input[name=FNAME]').next('small').html(admin_url.empty_fname);
            passd = false;
        } else {
            if (jQuery('input[name=FNAME]').val().length < 2) {
                jQuery('input[name=FNAME]').next('small').removeClass('d-none');
                jQuery('input[name=FNAME]').next('small').html(admin_url.error_fname);
                passd = false;
            } else {
                if (validAlphaExp(jQuery('input[name=FNAME]').val()) == false) {
                    jQuery('input[name=FNAME]').next('small').removeClass('d-none');
                    jQuery('input[name=FNAME]').next('small').html(admin_url.error_fname);
                    passd = false;
                } else {
                    jQuery('input[name=FNAME]').next('small').addClass('d-none');
                }
            }
        }

        if (jQuery('input[name=LNAME]').val() == '') {
            jQuery('input[name=LNAME]').next('small').removeClass('d-none');
            jQuery('input[name=LNAME]').next('small').html(admin_url.empty_lname);
            passd = false;
        } else {
            if (jQuery('input[name=LNAME]').val().length < 2) {
                jQuery('input[name=LNAME]').next('small').removeClass('d-none');
                jQuery('input[name=LNAME]').next('small').html(admin_url.error_lname);
                passd = false;
            } else {
                if (validAlphaExp(jQuery('input[name=LNAME]').val()) == false) {
                    jQuery('input[name=LNAME]').next('small').removeClass('d-none');
                    jQuery('input[name=LNAME]').next('small').html(admin_url.error_lname);
                    passd = false;
                } else {
                    jQuery('input[name=LNAME]').next('small').addClass('d-none');
                }
            }
        }

        if (jQuery('input[name=EMAIL]').val() == '') {
            jQuery('input[name=EMAIL]').next('small').removeClass('d-none');
            jQuery('input[name=EMAIL]').next('small').html(admin_url.empty_email);
            passd = false;
        } else {
            if (!isValidEmailAddress(jQuery('input[name=EMAIL]').val())) {
                jQuery('input[name=EMAIL]').next('small').removeClass('d-none');
                jQuery('input[name=EMAIL]').next('small').html(admin_url.error_email);
                passd = false;
            } else {
                jQuery('input[name=EMAIL]').next('small').addClass('d-none');
            }
        }

        if (jQuery('input[name=PHONE]').val() == '') {
            jQuery('input[name=PHONE]').next('small').removeClass('d-none');
            jQuery('input[name=PHONE]').next('small').html(admin_url.empty_phone);
            passd = false;
        } else {
            if (jQuery('input[name=PHONE]').val().length < 2) {
                jQuery('input[name=PHONE]').next('small').removeClass('d-none');
                jQuery('input[name=PHONE]').next('small').html(admin_url.error_phone);
                passd = false;
            } else {
                if (telephoneCheck(jQuery('input[name=PHONE]').val()) == false) {
                    jQuery('input[name=PHONE]').next('small').removeClass('d-none');
                    jQuery('input[name=PHONE]').next('small').html(admin_url.error_phone);
                    passd = false;
                } else {
                    jQuery('input[name=PHONE]').next('small').addClass('d-none');
                }
            }
        }

        if (jQuery('input[name=ZIPCODE]').val() == '') {
            jQuery('input[name=ZIPCODE]').next('small').removeClass('d-none');
            jQuery('input[name=ZIPCODE]').next('small').html(admin_url.empty_zipcode);
            passd = false;
        } else {
            if (jQuery('input[name=ZIPCODE]').val().length < 5) {
                jQuery('input[name=ZIPCODE]').next('small').removeClass('d-none');
                jQuery('input[name=ZIPCODE]').next('small').html(admin_url.error_zipcode);
                passd = false;
            } else {
                /*    if (jQuery.inArray(jQuery('input[name=ZIPCODE]').val(), zipcodes_arr) == -1) {
                    jQuery('input[name=ZIPCODE]').next('small').removeClass('d-none');
                    jQuery('input[name=ZIPCODE]').next('small').html(admin_url.not_found_zipcode);
                    passd = false;
                } else {*/
                jQuery('input[name=ZIPCODE]').next('small').addClass('d-none');
            }
        }
        //        }

        if (passd == true) {
            jQuery.ajax({
                type: 'POST',
                url: admin_url.ajax_url,
                data: {
                    action: 'custom_mailchimp_connector_add_subscriber',
                    info: jQuery('.form-mailchimp-modal').serialize()
                },
                beforeSend: function () {
                    jQuery('.custom-mailchimp-connector-response').html('<div class="lds-dual-ring"></div></div>');
                },
                success: function (response) {
                    if (response == true) {
                        setTimeout(function () {
                            location.href = admin_url.thanks_url;
                        }, 500);
                    } else {
                        jQuery('.custom-mailchimp-connector-response').html(response);
                    }
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        }
    });

}); /* end of as page load scripts */
