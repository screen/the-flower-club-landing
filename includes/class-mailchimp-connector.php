<?php

/* --------------------------------------------------------------
    CUSTOM PHP CLASS FOR MAILCHIMP FUNCTIONS
-------------------------------------------------------------- */
class Mailchimp_Connector {
    /* DECLARE */
    protected $api_key = '1';
    protected $api_prefix = '1';

    public function __construct() {
        /* GET CURRENT APIKEY */
        $apikey_mailchimp = get_option('flowerclub_mailchimp_settings');
        $this->api_key = $apikey_mailchimp['apikey'];
        /* GET CURRENT API PREFIX FOR SERVER ADDRESS */
        $api_prefixer = explode('-', $apikey_mailchimp['apikey']);
        $this->api_prefix = $api_prefixer[1];
    }

    /* MAIN CURL REQUEST */
    public function APICurlResponse($url, $request_type, $payload) {
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '.base64_encode( 'user:'. $this->api_key )
        );

        if( $request_type == 'GET' ) {
            if (!empty($payload)) {
                $url .= '?' . http_build_query($payload);
            }
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if( $request_type != 'GET' ) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        }
        $result = curl_exec($ch);

        return $result;
    }

    /* GET CURRENT AUDIENCE LISTS */
    public function GetCurrentLists() {
        $transient_lists = get_transient('mailchimp_lists');

        if ( !$transient_lists ) {
            $url = 'https://' . $this->api_prefix . '.api.mailchimp.com/3.0/lists/';
            $data = array(
                'fields' => 'lists',
                'count' => 'all'
            );

            $data_lists = $this->APICurlResponse($url, 'GET', $data);
            $data_lists = json_decode($data_lists);

            foreach ($data_lists->lists as $item) {
                $lists[] = array('id' => $item->id, 'name' => $item->name);
            }

            set_transient('mailchimp_lists', $lists, 15 * MINUTE_IN_SECONDS);
        } else {
            $lists = $transient_lists;
        }

        return $lists;
    }

    /* GET CURRENT LIST SEGMENTS */
    public function GetCurrentTags($list) {
        $transient_tags = get_transient('mailchimp_tags');

        if ( !$transient_tags ) {
            $url = 'https://' . $this->api_prefix . '.api.mailchimp.com/3.0/lists/'. $list .'/segments/';
            $data = array( );

            $data_lists = $this->APICurlResponse($url, 'GET', $data);
            $data_lists = json_decode($data_lists);
            foreach ($data_lists->segments as $item) {
                $lists[] = array('id' => $item->id, 'name' => $item->name);
            }
            set_transient('mailchimp_tags', $lists, 15 * MINUTE_IN_SECONDS);
        } else {
            $lists = $transient_tags;
        }

        return $lists;
    }

    public function AddUsertoList($info) {
        $url = 'https://' . $this->api_prefix . '.api.mailchimp.com/3.0/lists/' . $info['mailchimp_list'] . '/members/';
        $data = array(
            'email_address' => $info['EMAIL'],
            'status' => 'subscribed',
            'merge_fields' => array(
                'FNAME' => $info['FNAME'],
                'LNAME' => $info['LNAME'],
                'PHONE' => $info['PHONE'],
                'ZIPCODE' => $info['ZIPCODE'],
                'SOURCE' => $info['SOURCE'],
                'MEDIUM' => $info['MEDIUM'],
                'NAME' => $info['NAME']
            )
        );

        $response = $this->APICurlResponse($url, 'POST', $data);
        $response = json_decode($response);

        if ($response->status == 400) {
            $result = __('Ya el correo está agregado al boletín de flowerclub', 'flowerclub');
        } else {
            $segment_response = $this->AddUsertoSegment($info);
            $result = true;
        }
        return $result;
    }

    public function AddUsertoSegment($info) {
        $url = 'https://' . $this->api_prefix . '.api.mailchimp.com/3.0/lists/' . $info['mailchimp_list'] . '/segments/' . $info['mailchimp_tag'];
        $data = array(
            'members_to_add' => array($info['EMAIL'])
        );

        $response = $this->APICurlResponse($url, 'POST', $data);
        $response = json_decode($response);

        return $response;
    }

}

new Mailchimp_Connector();


/* --------------------------------------------------------------
    CUSTOM WIDGET FOR MAILCHIMP CONNECTOR
-------------------------------------------------------------- */

class custom_mailchimp_connector_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'custom_mailchimp_connector_widget',
            __('Conector de Mailchimp - Formulario Sencillo', 'flowerclub'),
            array( 'description' => __( 'Agrega un widget con un formulario sencillo.' ), )
        );
    }

    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $desc = $instance['description'];
        $mailchimp_list = $instance['mailchimp_list'];
        $mailchimp_tag = $instance['mailchimp_tag'];

        echo $args['before_widget'];

        ob_start();
?>
<div class="custom-mailchimp-connector-form-container">
    <?php if ( ! empty( $title ) ) { ?>
    <div class="custom-mailchimp-connector-title">
        <?php echo $args['before_title'] . $title . $args['after_title']; ?>
    </div>
    <?php } ?>
    <?php if ( ! empty( $desc ) ) { ?>
    <div class="custom-mailchimp-connector-desc">
        <?php echo $desc; ?>
    </div>
    <?php } ?>
    <form class="custom-mailchimp-connector">
        <input type="hidden" name="mailchimp_list" value="<?php echo $mailchimp_list; ?>" />
        <input type="hidden" name="mailchimp_tag" value="<?php echo $mailchimp_tag; ?>" />

        <input type="email" name="mailchimp_email" class="form-control" placeholder="<?php _e('Your email address', 'flowerclub'); ?>" />

        <p><?php _e("We protect your data, don't worry, we just want to be in touch to offer you the best of the best :)", 'flowerclub'); ?></p>

        <button class="btn btn-outline-secondary btn-submit" id="custom-mailchimp-connector-submit"><?php _e('Subscribe', 'flowerclub'); ?></button>


        <small class="mailchimp-error d-none"><?php _e('This email is invalid', 'flowerclub'); ?></small>
        <div class="custom-mailchimp-connector-response"></div>
    </form>
</div>



<?php
        $output = ob_get_contents();
        ob_end_clean();
        echo $output;
        echo $args['after_widget'];
    }

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'Suscribirse a nuestro newsletter', 'flowerclub' );
        }
        if ( isset( $instance[ 'description' ] ) ) {
            $desc = $instance[ 'description' ];
        }
        else {
            $desc = '';
        }
        if ( isset( $instance[ 'mailchimp_list' ] ) ) {
            $mailchimp_list = $instance[ 'mailchimp_list' ];
        }
        else {
            $mailchimp_list = '';
        }
        if ( isset( $instance[ 'mailchimp_tag' ] ) ) {
            $mailchimp_segment = $instance[ 'mailchimp_tag' ];
        }
        else {
            $mailchimp_segment = '';
        }
        $mailchimp = new Mailchimp_Connector();
?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">
        <?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'description' ); ?>">
        <?php _e( 'Texto descriptivo:' ); ?></label>
    <textarea class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" type="text"><?php echo esc_attr( $desc ); ?></textarea>
</p>
<p>
    <?php $lists = $mailchimp->GetCurrentLists(); ?>
    <label for="mailchimp_list">
        <?php _e( 'Lista de Mailchimp:' ); ?></label>
    <select id="mailchimp_list" name="<?php echo $this->get_field_name( 'mailchimp_list' ); ?>" class="widefat">
        <option value=""><?php _e('Seleccionar Lista', 'mailchimp'); ?></option>
        <?php foreach ($lists as $item) { ?>
        <option value="<?php echo $item['id']; ?>" <?php selected( $item['id'], $mailchimp_list ); ?>><?php echo $item['name']; ?></option>
        <?php } ?>
    </select>
</p>
<p>
    <?php $segments = $mailchimp->GetCurrentTags($mailchimp_list); ?>
    <label for="mailchimp_tag">
        <?php _e( 'Lista / Segmento:' ); ?></label>
    <select id="mailchimp_tag" name="<?php echo $this->get_field_name( 'mailchimp_tag' ); ?>" class="widefat">
        <option value=""><?php _e('Seleccionar Segmento', 'mailchimp'); ?></option>
        <?php foreach ($segments as $item) { ?>
        <option value="<?php echo $item['id']; ?>" <?php selected( $item['id'], $mailchimp_segment ); ?>><?php echo $item['name']; ?></option>
        <?php } ?>
    </select>
</p>
<?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
        $instance['mailchimp_list'] = ( ! empty( $new_instance['mailchimp_list'] ) ) ? strip_tags( $new_instance['mailchimp_list'] ) : '';
        $instance['mailchimp_tag'] = ( ! empty( $new_instance['mailchimp_tag'] ) ) ? strip_tags( $new_instance['mailchimp_tag'] ) : '';
        return $instance;
    }
}


add_action('wp_ajax_custom_mailchimp_connector_add_subscriber', 'custom_mailchimp_connector_add_subscriber_handler');
add_action('wp_ajax_nopriv_custom_mailchimp_connector_add_subscriber', 'custom_mailchimp_connector_add_subscriber_handler');

function custom_mailchimp_connector_add_subscriber_handler() {
    if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
        error_reporting( E_ALL );
        ini_set( 'display_errors', 1 );
    }
    parse_str($_POST['info'], $data);

    $mailchimp = new Mailchimp_Connector();
    $subscribe_response = $mailchimp->AddUsertoList($data);

    echo $subscribe_response;

    wp_die();
}
