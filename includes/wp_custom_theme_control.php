<?php
/* --------------------------------------------------------------
CUSTOM AREA FOR OPTIONS DATA - FLOWERCLUB
-------------------------------------------------------------- */

add_action( 'customize_register', 'flowerclub_customize_register' );

function flowerclub_customize_register( $wp_customize ) {
    $mailchimp = new Mailchimp_Connector();
    $data_mailchimp = get_option('flowerclub_mailchimp_settings');
    $lists = $mailchimp->GetCurrentLists();

    $wp_customize->add_section('flowerclub_mailchimp_settings', array(
        'title'    => __('Mailchimp', 'flowerclub'),
        'description' => __('Opciones de Mailchimp', 'flowerclub'),
        'priority' => 170,
    ));

    $wp_customize->add_setting('flowerclub_mailchimp_settings[apikey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'apikey', array(
        'type' => 'text',
        'label'    => __('Mailchimp API Key', 'flowerclub'),
        'description' => __( 'Add Mailchimp API Key' ),
        'section'  => 'flowerclub_mailchimp_settings',
        'settings' => 'flowerclub_mailchimp_settings[apikey]'
    ));



    $mailchimp_lists = array();
    foreach ($lists as $item) {
        $mailchimp_lists[$item['id']] = $item['name'];
    }

    $wp_customize->add_setting('flowerclub_mailchimp_settings[audience]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize, //Pass the $wp_customize object (required)
        'audience', //Set a unique ID for the control
        array(
            'label'      => __( 'Mailchimp Audience', 'parsmizban' ),
            'description' => __( 'Select the audience where you want to save this subscriber' ),
            'priority'   => 10,
            'section'  => 'flowerclub_mailchimp_settings',
            'settings' => 'flowerclub_mailchimp_settings[audience]',
            'type'    => 'select',
            'choices' => $mailchimp_lists
        )
    ) );

    $mailchimp_segments = array();
    if ($data_mailchimp['audience'] != '') {
        $segments = $mailchimp->GetCurrentTags($data_mailchimp['audience']);
        foreach ($segments as $item) {
            $mailchimp_segments[$item['id']] = $item['name'];
        }
    }

    $wp_customize->add_setting('flowerclub_mailchimp_settings[tag]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize, //Pass the $wp_customize object (required)
        'tag', //Set a unique ID for the control
        array(
            'label'      => __( 'Mailchimp Tag', 'parsmizban' ),
            'description' => __( 'Select the Tag where you want to save this subscriber' ),
            'priority'   => 10,
            'section'  => 'flowerclub_mailchimp_settings',
            'settings' => 'flowerclub_mailchimp_settings[tag]',
            'type'    => 'select',
            'choices' => $mailchimp_segments
        )
    ) );

    $wp_customize->add_setting( 'flowerclub_mailchimp_settings[thanks_page]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'              => 'option'
    ) );

    $wp_customize->add_control( 'thanks_page', array(
        'label'    => __( 'Select Page', 'textdomain' ),
        'label'    => __('Mailchimp Thanks Page', 'flowerclub'),
        'description' => __( 'Add page for thank the subscriber' ),
        'section'  => 'flowerclub_mailchimp_settings',
        'settings' => 'flowerclub_mailchimp_settings[thanks_page]',
        'type'     => 'dropdown-pages'
    ) );


    // HEADER
    $wp_customize->add_section('flowerclub_header_settings', array(
        'title'    => __('Header', 'flowerclub'),
        'description' => __('Header Options', 'flowerclub'),
        'priority' => 30
    ));

    $wp_customize->add_setting('flowerclub_header_settings[phone_number]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'phone_number', array(
        'type' => 'text',
        'label'    => __('Phone Number', 'flowerclub'),
        'description' => __( 'Add Phone Number with code for the link in header' ),
        'section'  => 'flowerclub_header_settings',
        'settings' => 'flowerclub_header_settings[phone_number]'
    ));

    $wp_customize->add_setting('flowerclub_header_settings[formatted_phone_number]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'formatted_phone_number', array(
        'type' => 'text',
        'label'    => __('Formatted Phone Number', 'flowerclub'),
        'description' => __( 'Add Phone Number in formatted form for header' ),
        'section'  => 'flowerclub_header_settings',
        'settings' => 'flowerclub_header_settings[formatted_phone_number]'
    ));

    $wp_customize->add_setting('flowerclub_header_settings[subscribe_url]', array(
        'default'           => '',
        'sanitize_callback' => 'flowerclub_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'subscribe_url', array(
        'label' => __( 'Subscribe Button URL', 'flowerclub' ),
        'description' => __( 'Add here the link for the subscribe button in header' ),
        'type' => 'url',
        'section' => 'flowerclub_header_settings',
        'settings' => 'flowerclub_header_settings[subscribe_url]'
    ));

    // FOOTER
    $wp_customize->add_section('flowerclub_footer_settings', array(
        'title'    => __('Footer', 'flowerclub'),
        'description' => __('Opciones del pie de página', 'flowerclub'),
        'priority' => 31,
    ));

    $wp_customize->add_setting('flowerclub_footer_settings[custom_html]', array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( new WP_Customize_Code_Editor_Control( $wp_customize, 'custom_html', array(
        'label'     => 'Additional JS',
        'code_type' => 'javascript',
        'section'  => 'flowerclub_footer_settings',
        'settings'   => 'flowerclub_footer_settings[custom_html]'
    ) ) );



    $wp_customize->add_section('flowerclub_cookie_settings', array(
        'title'    => __('Cookies', 'flowerclub'),
        'description' => __('Opciones de Cookies', 'flowerclub'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('flowerclub_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'flowerclub'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'flowerclub_cookie_settings',
        'settings' => 'flowerclub_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('flowerclub_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'flowerclub_cookie_settings',
        'settings' => 'flowerclub_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'flowerclub' ),
    ) );
}

function flowerclub_sanitize_url( $url ) {
    return esc_url_raw( $url );
}

add_action( 'wp_footer', 'prefix_customize_output', 100 );

function prefix_customize_output() {
    $footer_options = get_option('flowerclub_footer_settings');
    if ( $footer_options['custom_html'] === '' ) {
        return;
    }

?>
<script type="text/javascript">
    <?php echo $footer_options['custom_html'] . "\n"; ?>

</script>
<?php

}
