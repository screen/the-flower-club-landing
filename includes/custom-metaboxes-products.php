<?php

$cmb_product_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'product_metabox',
    'title'         => esc_html__( 'Product: Extra Info', 'flowerclub' ),
    'object_types'  => array( 'product' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_product_metabox->add_field( array(
    'id'      => $prefix . 'product_btn_text',
    'name'      => esc_html__( 'Button Text', 'flowerclub' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'flowerclub' ),
    'type'    => 'text'
) );

$cmb_product_metabox->add_field( array(
    'id'      => $prefix . 'product_btn_url',
    'name'      => esc_html__( 'Button URL', 'flowerclub' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'flowerclub' ),
    'type'    => 'text_url'
) );
