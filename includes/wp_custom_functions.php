<?php
/* IMAGES RESPONSIVE ON ATTACHMENT IMAGES */
function image_tag_class($class) {
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class' );

/* ADD CONTENT WIDTH FUNCTION */

function flowerclub_content_width() {
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters( 'flowerclub_content_width', 1170 );
}
add_action( 'after_setup_theme', 'flowerclub_content_width', 0 );

/* ADD CONTENT WIDTH FUNCTION */

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
    $classes[] = 'nav-item';
    if( in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

// let's add our custom class to the actual link tag

function atg_menu_classes($classes, $item, $args) {
    if($args->theme_location == 'topnav') {
        $classes[] = 'nav-link';
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'atg_menu_classes', 1, 3);

function add_menuclass($ulclass) {
    return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function flowerclub_pingback_header() {
    if ( is_singular() && pings_open() ) {
        printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
    }
}
add_action( 'wp_head', 'flowerclub_pingback_header' );

/**
 * WIDGET
 */
// Register and load the widget
function wpb_load_widget() {
    register_widget( 'flowerclub_social' );
    register_widget( 'flowerclub_app_widget' );

}
add_action( 'widgets_init', 'wpb_load_widget' );

// Creating the widget
class flowerclub_social extends WP_Widget {

    function __construct() {
        parent::__construct(
            'flowerclub_social',
            __('>> Social Networks', 'flowerclub'),
            array( 'description' => __( 'Widget for social networks on footer', 'flowerclub' ), )
        );
    }

    // Creating widget front-end
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $facebook = $instance['facebook'];
        $instagram = $instance['instagram'];
        $youtube = $instance['youtube'];
        $twitter = $instance['twitter'];

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];

        if ($facebook != '') { ?>
<a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>
<?php }

        if ($instagram != '') { ?>
<a href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a>
<?php }

        if ($youtube != '') { ?>
<a href="<?php echo $youtube; ?>" target="_blank"><i class="fa fa-youtube-play"></i></a>
<?php }

        if ($twitter != '') { ?>
<a href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
<?php }
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'Follow Us', 'flowerclub' );
        }

        if ( isset( $instance[ 'facebook' ] ) ) {
            $facebook = $instance[ 'facebook' ];
        }
        else {
            $facebook = '';
        }

        if ( isset( $instance[ 'twitter' ] ) ) {
            $twitter = $instance[ 'twitter' ];
        }
        else {
            $twitter = '';
        }

        if ( isset( $instance[ 'instagram' ] ) ) {
            $instagram = $instance[ 'instagram' ];
        }
        else {
            $instagram = '';
        }

        if ( isset( $instance[ 'youtube' ] ) ) {
            $youtube = $instance[ 'youtube' ];
        }
        else {
            $youtube = '';
        }

        // Widget admin form
?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e( 'Facebook URL:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" type="text" value="<?php echo esc_attr( $facebook ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'instagram' ); ?>"><?php _e( 'Instagram URL:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'instagram' ); ?>" name="<?php echo $this->get_field_name( 'instagram' ); ?>" type="text" value="<?php echo esc_attr( $instagram ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e( 'Twitter:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" type="text" value="<?php echo esc_attr( $twitter ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'youtube' ); ?>"><?php _e( 'Youtube:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" type="text" value="<?php echo esc_attr( $youtube ); ?>" />
</p>

<?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        $instance['facebook'] = ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';

        $instance['twitter'] = ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';

        $instance['instagram'] = ( ! empty( $new_instance['instagram'] ) ) ? strip_tags( $new_instance['instagram'] ) : '';

        $instance['youtube'] = ( ! empty( $new_instance['youtube'] ) ) ? strip_tags( $new_instance['youtube'] ) : '';
        return $instance;
    }
} // Class wpb_widget ends here


// Creating the widget
class flowerclub_app_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'flowerclub_app_widget',
            __('>> App Shop Download', 'flowerclub'),
            array( 'description' => __( 'Widget for app download on footer', 'flowerclub' ), )
        );
    }

    // Creating widget front-end
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $appstore = $instance['appstore'];
        $googleplay = $instance['googleplay'];

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];
        $lang = get_bloginfo("language");
        if ($lang != 'en-US') { $class = '-esp'; } else { $class = ''; }
        if ($appstore != '') { ?>
<a href="<?php echo $appstore; ?>" target="_blank">
    <span class="app-gallery-btn app-gallery-btn-apple<?php echo $class; ?>"></span>
</a>
<?php }

        if ($googleplay != '') { ?>
<a href="<?php echo $googleplay; ?>" target="_blank">
    <span class="app-gallery-btn app-gallery-btn-google<?php echo $class; ?>"></span>
</a>
<?php }

        echo $args['after_widget'];
    }

    // Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'Download and Install the APP', 'flowerclub' );
        }

        if ( isset( $instance[ 'appstore' ] ) ) {
            $appstore = $instance[ 'appstore' ];
        }
        else {
            $appstore = '';
        }

        if ( isset( $instance[ 'googleplay' ] ) ) {
            $googleplay = $instance[ 'googleplay' ];
        }
        else {
            $googleplay = '';
        }
        // Widget admin form
?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'appstore' ); ?>"><?php _e( 'AppStore URL:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'appstore' ); ?>" name="<?php echo $this->get_field_name( 'appstore' ); ?>" type="text" value="<?php echo esc_attr( $appstore ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'googleplay' ); ?>"><?php _e( 'Google Play URL:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'googleplay' ); ?>" name="<?php echo $this->get_field_name( 'googleplay' ); ?>" type="text" value="<?php echo esc_attr( $googleplay ); ?>" />
</p>

<?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        $instance['appstore'] = ( ! empty( $new_instance['appstore'] ) ) ? strip_tags( $new_instance['appstore'] ) : '';

        $instance['googleplay'] = ( ! empty( $new_instance['googleplay'] ) ) ? strip_tags( $new_instance['googleplay'] ) : '';

        return $instance;
    }
} // Class wpb_widget ends here
