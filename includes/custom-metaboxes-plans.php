<?php

/* 1.- MAIN SLIDER */
$cmb_plans_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'plans_metabox',
    'title'         => esc_html__( 'Plans: Extra Info', 'flowerclub' ),
    'object_types'  => array( 'plans' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );


$cmb_plans_metabox->add_field( array(
    'id'      => $prefix . 'plans_price',
    'name'      => esc_html__( 'Price', 'flowerclub' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'flowerclub' ),
    'type'    => 'text',
    'options' => array(),
) );

$cmb_plans_metabox->add_field( array(
    'id'      => $prefix . 'plans_price_message',
    'name'      => esc_html__( 'Message below Price', 'flowerclub' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'flowerclub' ),
    'type'    => 'text',
    'options' => array(),
) );

$cmb_plans_metabox->add_field( array(
    'id'   => $prefix . 'plans_icon',
    'name' => esc_html__('Icon', 'flowerclub'),
    'desc' => esc_html__('Select a Icon image for this Section.', 'flowerclub'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Upload Icon',
        'remove_image_text' => 'Remove Icon',
        'file_text' => 'Icon:',
        'file_download_text' => 'Download',
        'remove_text' => 'Remove'
    )
) );
$group_field_id = $cmb_plans_metabox->add_field( array(
    'id'          => $prefix . 'plans_featured_group',
    'type'        => 'group',
    'description' => __( 'Features', 'flowerclub' ),
    'options'     => array(
        'group_title'       => __( 'Items {#}', 'flowerclub' ),
        'add_button'        => __( 'Add other item', 'flowerclub' ),
        'remove_button'     => __( 'Remove item', 'flowerclub' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro que quiere eliminar este item?', 'flowerclub' )
    )
) );


$cmb_plans_metabox->add_group_field( $group_field_id, array(
    'id'   => 'features',
    'name' => esc_html__('Feature', 'flowerclub'),
    'desc' => esc_html__("Ingrese un texto descriptivo del Button 1 - Text", 'flowerclub'),
    'type' => 'text'
) );


