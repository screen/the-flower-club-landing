<?php


function flowerclub_custom_post_type() {

    $labels = array(
        'name'                  => _x( 'Plans', 'Post Type General Name', 'flowerclub' ),
        'singular_name'         => _x( 'Plan', 'Post Type Singular Name', 'flowerclub' ),
        'menu_name'             => __( 'Plans', 'flowerclub' ),
        'name_admin_bar'        => __( 'Plans', 'flowerclub' ),
        'archives'              => __( 'Plans Archive', 'flowerclub' ),
        'attributes'            => __( 'Plan Attributes', 'flowerclub' ),
        'parent_item_colon'     => __( 'Parent Plan:', 'flowerclub' ),
        'all_items'             => __( 'All Plans', 'flowerclub' ),
        'add_new_item'          => __( 'Add New Plan', 'flowerclub' ),
        'add_new'               => __( 'Add New', 'flowerclub' ),
        'new_item'              => __( 'New Plan', 'flowerclub' ),
        'edit_item'             => __( 'Edit Plan', 'flowerclub' ),
        'update_item'           => __( 'Update Plan', 'flowerclub' ),
        'view_item'             => __( 'View Plan', 'flowerclub' ),
        'view_items'            => __( 'View Plans', 'flowerclub' ),
        'search_items'          => __( 'Search Plans', 'flowerclub' ),
        'not_found'             => __( 'No results', 'flowerclub' ),
        'not_found_in_trash'    => __( 'No results in trash', 'flowerclub' ),
        'featured_image'        => __( 'Featured Image', 'flowerclub' ),
        'set_featured_image'    => __( 'Set Featured Image', 'flowerclub' ),
        'remove_featured_image' => __( 'Remove Featured Image', 'flowerclub' ),
        'use_featured_image'    => __( 'Use as Featured Image', 'flowerclub' ),
        'insert_into_item'      => __( 'Insert into Plan', 'flowerclub' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Plan', 'flowerclub' ),
        'items_list'            => __( 'Listado de Plans', 'flowerclub' ),
        'items_list_navigation' => __( 'Navegación de Listado de Plans', 'flowerclub' ),
        'filter_items_list'     => __( 'Filtro de Listado de Plans', 'flowerclub' ),
    );
    $args = array(
        'label'                 => __( 'Plan', 'flowerclub' ),
        'description'           => __( 'Plans ', 'flowerclub' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-list-view',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => true,
    );
    register_post_type( 'plans', $args );


    $labels = array(
        'name'                  => _x( 'Products', 'Post Type General Name', 'flowerclub' ),
        'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'flowerclub' ),
        'menu_name'             => __( 'Products', 'flowerclub' ),
        'name_admin_bar'        => __( 'Products', 'flowerclub' ),
        'archives'              => __( 'Products Archive', 'flowerclub' ),
        'attributes'            => __( 'Product Attributes', 'flowerclub' ),
        'parent_item_colon'     => __( 'Parent Product:', 'flowerclub' ),
        'all_items'             => __( 'All Products', 'flowerclub' ),
        'add_new_item'          => __( 'Add New Product', 'flowerclub' ),
        'add_new'               => __( 'Add New', 'flowerclub' ),
        'new_item'              => __( 'New Product', 'flowerclub' ),
        'edit_item'             => __( 'Edit Product', 'flowerclub' ),
        'update_item'           => __( 'Update Product', 'flowerclub' ),
        'view_item'             => __( 'View Product', 'flowerclub' ),
        'view_items'            => __( 'View Products', 'flowerclub' ),
        'search_items'          => __( 'Search Plans', 'flowerclub' ),
        'not_found'             => __( 'No results', 'flowerclub' ),
        'not_found_in_trash'    => __( 'No results in trash', 'flowerclub' ),
        'featured_image'        => __( 'Featured Image', 'flowerclub' ),
        'set_featured_image'    => __( 'Set Featured Image', 'flowerclub' ),
        'remove_featured_image' => __( 'Remove Featured Image', 'flowerclub' ),
        'use_featured_image'    => __( 'Use as Featured Image', 'flowerclub' ),
        'insert_into_item'      => __( 'Insert into Product', 'flowerclub' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Product', 'flowerclub' ),
        'items_list'            => __( 'Listado de Products', 'flowerclub' ),
        'items_list_navigation' => __( 'Navegación de Listado de Products', 'flowerclub' ),
        'filter_items_list'     => __( 'Filtro de Listado de Products', 'flowerclub' ),
    );
    $args = array(
        'label'                 => __( 'Products', 'flowerclub' ),
        'description'           => __( 'Products ', 'flowerclub' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 6,
        'taxonomies'            => array('product_cat'),
        'menu_icon'             => 'dashicons-archive',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => true,
    );
    register_post_type( 'product', $args );

    // Register Custom Taxonomy
    $labels = array(
        'name'                       => _x( 'Categories', 'Taxonomy General Name', 'flowerclub' ),
        'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'flowerclub' ),
        'menu_name'                  => __( 'Categories', 'flowerclub' ),
        'all_items'                  => __( 'All Categories', 'flowerclub' ),
        'parent_item'                => __( 'Parent Category', 'flowerclub' ),
        'parent_item_colon'          => __( 'Parent Category:', 'flowerclub' ),
        'new_item_name'              => __( 'New Category Name', 'flowerclub' ),
        'add_new_item'               => __( 'Add New Category', 'flowerclub' ),
        'edit_item'                  => __( 'Edit Category', 'flowerclub' ),
        'update_item'                => __( 'Update Category', 'flowerclub' ),
        'view_item'                  => __( 'View Category', 'flowerclub' ),
        'separate_items_with_commas' => __( 'Separate Categories with commas', 'flowerclub' ),
        'add_or_remove_items'        => __( 'Add or remove Categories', 'flowerclub' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'flowerclub' ),
        'popular_items'              => __( 'Popular Categories', 'flowerclub' ),
        'search_items'               => __( 'Search Categories', 'flowerclub' ),
        'not_found'                  => __( 'Not Found', 'flowerclub' ),
        'no_terms'                   => __( 'No Categories', 'flowerclub' ),
        'items_list'                 => __( 'Categories list', 'flowerclub' ),
        'items_list_navigation'      => __( 'Categories list navigation', 'flowerclub' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'show_in_rest'               => true,
    );
    register_taxonomy( 'product_cat', array( 'product' ), $args );




}
add_action( 'init', 'flowerclub_custom_post_type', 0 );
