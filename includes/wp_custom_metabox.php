<?php

function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}

add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );


function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );

add_action( 'cmb2_admin_init', 'flowerclub_register_custom_metabox' );
function flowerclub_register_custom_metabox() {
    $prefix = 'tfc_';


    $categories_array = array();
    $categories = get_terms('product_cat', array('hide_empty' => 'false'));
    foreach( $categories as $category ) {
        $categories_array[$category->term_id] = $category->name;
    }

    //// 1.- MAIN SLIDER  ////
    $cmb_landing_slider = new_cmb2_box( array(
        'id'            => $prefix . 'landing_slider',
        'title'         => esc_html__( 'Landing: Main Slider', 'flowerclub' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'slug', 'value' => 'landing' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => true
    ) );

    $cmb_landing_slider->add_field( array(
        'id'   => $prefix . 'landing_slider_activate',
        'name' => esc_html__( 'Activate Section', 'flowerclub' ),
        'desc' => esc_html__( 'Check this to activate this section', 'flowerclub' ),
        'type' => 'checkbox'
    ) );

    $group_field_id = $cmb_landing_slider->add_field( array(
        'id'          => $prefix . 'landing_slider_group',
        'type'        => 'group',
        'description' => __( 'Items', 'flowerclub' ),
        'options'     => array(
            'group_title'       => __( 'Slide {#}', 'flowerclub' ),
            'add_button'        => __( 'Add other Slide', 'flowerclub' ),
            'remove_button'     => __( 'Remove Slide', 'flowerclub' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'are you sure to delete this slide?', 'flowerclub' )
        )
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'bg',
        'name' => esc_html__('Slide Background', 'flowerclub'),
        'desc' => esc_html__('Select a descriptive background for this slide.', 'flowerclub'),
        'type' => 'file',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => 'Upload Image',
            'remove_image_text' => 'Remove Image',
            'file_text' => 'Image:',
            'file_download_text' => 'Download',
            'remove_text' => 'Remove'
        )
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'mobile_bg',
        'name' => esc_html__('Mobile Background', 'flowerclub'),
        'desc' => esc_html__('Select a descriptive background for this slide.', 'flowerclub'),
        'type' => 'file',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => 'Upload Image',
            'remove_image_text' => 'Remove Image',
            'file_text' => 'Image:',
            'file_download_text' => 'Download',
            'remove_text' => 'Remove'
        )
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name' => esc_html__('Slide Text', 'flowerclub'),
        'desc' => esc_html__("Insert a descriptive text for this slide.", 'flowerclub'),
        'type' => 'wysiwyg'
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'button_1',
        'name' => esc_html__('Button 1 - Text', 'flowerclub'),
        'desc' => esc_html__("Insert a descriptive text for this Button 1", 'flowerclub'),
        'type' => 'text'
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'button_2',
        'name' => esc_html__('Button 2 - Text', 'flowerclub'),
        'desc' => esc_html__("Insert a descriptive text for this Button 2", 'flowerclub'),
        'type' => 'text'
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'button_1_url',
        'name' => esc_html__('Button 1 - URL', 'flowerclub'),
        'desc' => esc_html__("Insert a descriptive URL for this Button 1", 'flowerclub'),
        'type' => 'text_url'
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'button_2_url',
        'name' => esc_html__('Button 2 - URL', 'flowerclub'),
        'desc' => esc_html__("Insert a descriptive URL for this Button 2", 'flowerclub'),
        'type' => 'text_url'
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'invert',
        'name' => esc_html__('Invert Row', 'flowerclub'),
        'desc' => esc_html__("Select if you want to invert this row.", 'flowerclub'),
        'type' => 'checkbox'
    ) );

    $cmb_landing_slider->add_group_field( $group_field_id, array(
        'id'   => 'special_banner',
        'name' => esc_html__('Special Banner', 'flowerclub'),
        'desc' => esc_html__("Select if this slide has a different function.", 'flowerclub'),
        'type' => 'checkbox'
    ) );

    /* 2.- HOW IT WORKS SECTION */
    $cmb_landing_works = new_cmb2_box( array(
        'id'            => $prefix . 'landing_works',
        'title'         => esc_html__( 'Landing: How it Works - Section ', 'flowerclub' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on' => array( 'key' => 'slug', 'value' => 'landing' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => true
    ) );

    $cmb_landing_works->add_field( array(
        'id'   => $prefix . 'landing_works_activate',
        'name' => esc_html__( 'Activate Section', 'flowerclub' ),
        'desc' => esc_html__( 'Check this to activate this section', 'flowerclub' ),
        'type' => 'checkbox'
    ) );

    $cmb_landing_works->add_field( array(
        'id'   => $prefix . 'works_background',
        'name' => esc_html__('Section Background', 'flowerclub'),
        'desc' => esc_html__('Select a Background image for this Section.', 'flowerclub'),
        'type' => 'file',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => 'Upload Image',
            'remove_image_text' => 'Remove Image',
            'file_text' => 'Image:',
            'file_download_text' => 'Download',
            'remove_text' => 'Remove'
        )
    ) );

    $cmb_landing_works->add_field( array(
        'id'      => $prefix . 'works_title',
        'name'      => esc_html__( 'Section Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Section', 'flowerclub' ),
        'type'    => 'text',
        'options' => array(),
    ) );

    $cmb_landing_works->add_field( array(
        'id'      => $prefix . 'works_description',
        'name'      => esc_html__( 'Section Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Section', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'wpautop' => true, // use wpautop?
            'media_buttons' => true, // show insert/upload button(s)
            'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
            'textarea_rows' => get_option('default_post_edit_rows', 4), // rows="..."
            'tabindex' => '',
            'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
            'editor_class' => '', // add extra class(es) to the editor textarea
            'teeny' => false, // output the minimal editor config used in Press This
            'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
            'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
            'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ),
    ) );

    $group_field_id = $cmb_landing_works->add_field( array(
        'id'          => $prefix . 'landing_works_group',
        'type'        => 'group',
        'description' => __( 'Items', 'flowerclub' ),
        'options'     => array(
            'group_title'       => __( 'Items {#}', 'flowerclub' ),
            'add_button'        => __( 'Add other item', 'flowerclub' ),
            'remove_button'     => __( 'Remove item', 'flowerclub' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to erase this item?', 'flowerclub' )
        )
    ) );


    $cmb_landing_works->add_group_field( $group_field_id, array(
        'id'   => 'icon',
        'name' => esc_html__('Item Icon', 'flowerclub'),
        'desc' => esc_html__('Select an icon fors this item.', 'flowerclub'),
        'type' => 'file',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => 'Upload Image',
            'remove_image_text' => 'Remove Image',
            'file_text' => 'Image:',
            'file_download_text' => 'Download',
            'remove_text' => 'Remove'
        )
    ) );

    $cmb_landing_works->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name' => esc_html__('Item Description', 'flowerclub'),
        'desc' => esc_html__("Insert a descriptive text for this item.", 'flowerclub'),
        'type' => 'wysiwyg'
    ) );

    /* BUTTONS */
    $group_field_id = $cmb_landing_works->add_field( array(
        'id'          => $prefix . 'landing_works_btn_group',
        'type'        => 'group',
        'description' => __( 'Buttons', 'flowerclub' ),
        'options'     => array(
            'group_title'       => __( 'Button {#}', 'flowerclub' ),
            'add_button'        => __( 'Add other Button', 'flowerclub' ),
            'remove_button'     => __( 'Remove Button', 'flowerclub' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to erase this button?', 'flowerclub' )
        )
    ) );

    $cmb_landing_works->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name' => esc_html__('Button URL', 'flowerclub'),
        'desc' => esc_html__("Insert an URL for this Button", 'flowerclub'),
        'type' => 'text_url'
    ) );

    $cmb_landing_works->add_field( array(
        'id'      => $prefix . 'works_btn_title',
        'name'      => esc_html__( 'Button Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Button', 'flowerclub' ),
        'type'    => 'text',
        'options' => array()
    ) );

    $cmb_landing_works->add_field( array(
        'id'      => $prefix . 'works_btn_url',
        'name'      => esc_html__( 'Button URL', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert an URL for this Button', 'flowerclub' ),
        'type'    => 'text_url'
    ) );


    $cmb_landing_works->add_field( array(
        'id'      => $prefix . 'works_btn_description',
        'name'      => esc_html__( 'Button Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Button', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array()
    ) );


    /* 3.- PLANS SECTION */
    $cmb_landing_plans = new_cmb2_box( array(
        'id'            => $prefix . 'landing_plans',
        'title'         => esc_html__( 'Landing: Plans - Section ', 'flowerclub' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on' => array( 'key' => 'slug', 'value' => 'landing' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => true
    ) );

    $cmb_landing_plans->add_field( array(
        'id'   => $prefix . 'landing_plans_activate',
        'name' => esc_html__( 'Activate Section', 'flowerclub' ),
        'desc' => esc_html__( 'Check this to activate this section', 'flowerclub' ),
        'type' => 'checkbox'
    ) );

    $cmb_landing_plans->add_field( array(
        'id'      => $prefix . 'plans_title',
        'name'      => esc_html__( 'Section Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Section', 'flowerclub' ),
        'type'    => 'text',
        'options' => array()
    ) );

    $cmb_landing_plans->add_field( array(
        'id'      => $prefix . 'plans_description',
        'name'      => esc_html__( 'Section Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Section', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array()
    ) );

    $cmb_landing_plans->add_field( array(
        'id'      => $prefix . 'plans_btn_title',
        'name'      => esc_html__( 'Button Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Button', 'flowerclub' ),
        'type'    => 'text',
        'options' => array()
    ) );

    $cmb_landing_plans->add_field( array(
        'id'      => $prefix . 'plans_btn_url',
        'name'      => esc_html__( 'Button URL', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert an URL for this Button', 'flowerclub' ),
        'type'    => 'text_url'
    ) );


    $cmb_landing_plans->add_field( array(
        'id'      => $prefix . 'plans_btn_description',
        'name'      => esc_html__( 'Button Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Button', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array()
    ) );

    /* 4.- CATALOG SECTION */
    $cmb_landing_catalog = new_cmb2_box( array(
        'id'            => $prefix . 'landing_catalog',
        'title'         => esc_html__( 'Landing: Catalog - Section ', 'flowerclub' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'slug', 'value' => 'landing' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => true
    ) );

    $cmb_landing_catalog->add_field( array(
        'id'   => $prefix . 'landing_catalog_activate',
        'name' => esc_html__( 'Activate Section', 'flowerclub' ),
        'desc' => esc_html__( 'Check this to activate this section', 'flowerclub' ),
        'type' => 'checkbox'
    ) );


    $cmb_landing_catalog->add_field( array(
        'id'      => $prefix . 'catalog_title',
        'name'      => esc_html__( 'Section Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Section', 'flowerclub' ),
        'type'    => 'text',
        'options' => array()
    ) );

    $cmb_landing_catalog->add_field( array(
        'id'      => $prefix . 'catalog_description',
        'name'      => esc_html__( 'Section Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Section', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array()
    ) );

    $cmb_landing_catalog->add_field( array(
        'id'      => $prefix . 'catalog_embed',
        'name'      => esc_html__( 'Embed Code', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a Embed code to be loaded in a Modal', 'flowerclub' ),
        'type'    => 'textarea_code',
    ) );


    $cmb_landing_catalog->add_field( array(
        'id'      => $prefix . 'catalog_btn_title',
        'name'      => esc_html__( 'Button Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Button', 'flowerclub' ),
        'type'    => 'text',
        'options' => array()
    ) );

    $cmb_landing_catalog->add_field( array(
        'id'      => $prefix . 'catalog_btn_description',
        'name'      => esc_html__( 'Button Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Button', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array()
    ) );


    $cmb_landing_catalog->add_field( array(
        'id'            => $prefix . 'catalog_category_order',
        'name'          => __( 'Categories Order', 'flowerclub' ),
        'desc'          => __( 'Save order of categories in Landing', 'flowerclub' ),
        'type'          => 'order',
        // 'inline'        => true,
        'options'       => $categories_array
    ) );

    /* 5.- BLOG SECTION */
    $cmb_landing_blog = new_cmb2_box( array(
        'id'            => $prefix . 'landing_blog',
        'title'         => esc_html__( 'Landing: Blog - Section ', 'flowerclub' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on' => array( 'key' => 'slug', 'value' => 'landing' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => true
    ) );

    $cmb_landing_blog->add_field( array(
        'id'   => $prefix . 'landing_blog_activate',
        'name' => esc_html__( 'Activate Section', 'flowerclub' ),
        'desc' => esc_html__( 'Check this to activate this section', 'flowerclub' ),
        'type' => 'checkbox'
    ) );

    $cmb_landing_blog->add_field( array(
        'id'      => $prefix . 'blog_title',
        'name'      => esc_html__( 'Section Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Section', 'flowerclub' ),
        'type'    => 'text',
        'options' => array(),
    ) );

    $cmb_landing_blog->add_field( array(
        'id'   => $prefix . 'blog_background',
        'name' => esc_html__('Blog Background', 'flowerclub'),
        'desc' => esc_html__('Select a background image for this Section.', 'flowerclub'),
        'type' => 'file',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => 'Upload Image',
            'remove_image_text' => 'Remove Image',
            'file_text' => 'Image:',
            'file_download_text' => 'Download',
            'remove_text' => 'Remove'
        )
    ) );

    $group_field_id = $cmb_landing_blog->add_field( array(
        'id'          => $prefix . 'landing_blog_group',
        'type'        => 'group',
        'description' => __( 'Items', 'flowerclub' ),
        'options'     => array(
            'group_title'       => __( 'Item {#}', 'flowerclub' ),
            'add_button'        => __( 'Add other Item', 'flowerclub' ),
            'remove_button'     => __( 'Remove Item', 'flowerclub' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to erase this button?', 'flowerclub' )
        )
    ) );


    $cmb_landing_blog->add_group_field( $group_field_id, array(
        'id'   => 'image',
        'name' => esc_html__('Image', 'flowerclub'),
        'desc' => esc_html__('Select an image for this item.', 'flowerclub'),
        'type' => 'file',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => 'Upload Image',
            'remove_image_text' => 'Remove Image',
            'file_text' => 'Image:',
            'file_download_text' => 'Download',
            'remove_text' => 'Remove'
        )
    ) );

    $cmb_landing_blog->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name' => esc_html__('Title', 'flowerclub'),
        'desc' => esc_html__("Insert an title for this item", 'flowerclub'),
        'type' => 'text'
    ) );

    $cmb_landing_blog->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name' => esc_html__('Description', 'flowerclub'),
        'desc' => esc_html__("Insert an URL for this item", 'flowerclub'),
        'type' => 'wysiwyg'
    ) );

    $cmb_landing_blog->add_field( array(
        'id'      => $prefix . 'blog_btn_title',
        'name'      => esc_html__( 'Button Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Button', 'flowerclub' ),
        'type'    => 'text',
        'options' => array()
    ) );

    $cmb_landing_blog->add_field( array(
        'id'      => $prefix . 'blog_url',
        'name'      => esc_html__( 'Button URL', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert an URL for this Button', 'flowerclub' ),
        'type'    => 'text_url'
    ) );


    $cmb_landing_blog->add_field( array(
        'id'      => $prefix . 'blog_btn_description',
        'name'      => esc_html__( 'Button Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Button', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array()
    ) );

    /* 6.- HERO / DESCANSO SECTION */
    $cmb_landing_descanso = new_cmb2_box( array(
        'id'            => $prefix . 'landing_hero',
        'title'         => esc_html__( 'Landing: Hero - Section ', 'flowerclub' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on' => array( 'key' => 'slug', 'value' => 'landing' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => true
    ) );

    $cmb_landing_descanso->add_field( array(
        'id'   => $prefix . 'landing_hero_activate',
        'name' => esc_html__( 'Activate Section', 'flowerclub' ),
        'desc' => esc_html__( 'Check this to activate this section', 'flowerclub' ),
        'type' => 'checkbox'
    ) );

    $cmb_landing_descanso->add_field( array(
        'id'      => $prefix . 'hero_title',
        'name'      => esc_html__( 'Section Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Section', 'flowerclub' ),
        'type'    => 'text',
        'options' => array(),
    ) );

    $cmb_landing_descanso->add_field( array(
        'id'      => $prefix . 'hero_desc',
        'name'      => esc_html__( 'Section Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Section', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );

    $cmb_landing_descanso->add_field( array(
        'id'   => $prefix . 'hero_img',
        'name' => esc_html__('Hero Image', 'flowerclub'),
        'desc' => esc_html__('Select a Descriptive image for this Section.', 'flowerclub'),
        'type' => 'file',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => 'Upload Image',
            'remove_image_text' => 'Remove Image',
            'file_text' => 'Image:',
            'file_download_text' => 'Download',
            'remove_text' => 'Remove'
        )
    ) );

    $cmb_landing_descanso->add_field( array(
        'id'   => $prefix . 'hero_background',
        'name' => esc_html__('Hero Background', 'flowerclub'),
        'desc' => esc_html__('Select a background image for this Section.', 'flowerclub'),
        'type' => 'file',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => 'Upload Image',
            'remove_image_text' => 'Remove Image',
            'file_text' => 'Image:',
            'file_download_text' => 'Download',
            'remove_text' => 'Remove'
        )
    ) );

    $cmb_landing_descanso->add_field( array(
        'id'      => $prefix . 'hero_btn_title',
        'name'      => esc_html__( 'Button Title', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive title for this Button', 'flowerclub' ),
        'type'    => 'text',
        'options' => array()
    ) );

    $cmb_landing_descanso->add_field( array(
        'id'      => $prefix . 'hero_btn_url',
        'name'      => esc_html__( 'Button URL', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert an URL for this Button', 'flowerclub' ),
        'type'    => 'text_url'
    ) );


    $cmb_landing_descanso->add_field( array(
        'id'      => $prefix . 'hero_btn_description',
        'name'      => esc_html__( 'Button Description', 'flowerclub' ),
        'desc'      => esc_html__( 'Insert a descriptive text for this Button', 'flowerclub' ),
        'type'    => 'wysiwyg',
        'options' => array()
    ) );



    /* PLANS METABOXES */
    include_once('custom-metaboxes-plans.php');
    include_once('custom-metaboxes-products.php');
}
