<!DOCTYPE html>
<html <?php language_attributes() ?>>

    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#D40000" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#D40000" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="The Flower Club & Harmony" />
        <meta name="copyright" content="https://theflowerclub.net/" />
        <meta name="geo.position" content="25.8097781,-80.3092481" />
        <meta name="ICBM" content="25.8097781,-80.3092481" />
        <meta name="geo.region" content="US" />
        <meta name="geo.placename" content="3200 NW 67 Ave. Building 3. Suite 340 Fl. 33122" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!-->
        <html <?php language_attributes(); ?> class="no-js" />
        <!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <?php $header_options = get_option('flowerclub_header_settings'); ?>

    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJXHW4S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <header id="sticker" class="container-fluid p-0" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row no-gutters">
                <div class="the-header col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="container-fluid">
                        <div class="row align-items-center justify-content-center">
                            <div class="header-container header-container-left col-xl-5 col-lg-4 col-md-5 col-sm-5 col-8">
                                <a class="navbar-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                                    <?php ?> <?php $custom_logo_id = get_theme_mod( 'custom_logo' ); ?>
                                    <?php $image = wp_get_attachment_image_src( $custom_logo_id , 'full' ); ?>
                                    <?php if (!empty($image)) { ?>
                                    <img src="<?php echo $image[0];?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-logo" />
                                    <?php } ?>
                                </a>
                            </div>
                            <div class="header-container header-container-right col-xl-7 col-lg-8 col-md-7 col-sm-7 col-4">
                                <div class="row align-items-center justify-content-end">
                                    <div class="header-left-item col-xl-6 col-lg-6 col-md-9 col-sm-9 d-xl-flex d-lg-flex d-md-none d-sm-flex d-none">
                                        <div>
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon-call.png" alt="Call Us" class="img-fluid" />
                                        </div>
                                        <div>
                                            <h4><?php _e('Give us a call anytime!', 'flowerclub'); ?></h4>
                                            <a href="tel:<?php echo $header_options['phone_number']; ?>" title="<?php _e('Call us for assistance!', 'flowerclub'); ?>"><?php echo $header_options['formatted_phone_number']; ?></a>
                                        </div>
                                    </div>
                                    <div class="header-right-item col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-md-none d-sm-none d-none">
                                        <?php if ($header_options['subscribe_url'] != '') { ?>
                                        <a href="<?php echo $header_options['subscribe_url']; ?>" class="btn btn-md btn-subscribe" target="_blank"><?php _e('Subscribe Today!', 'flowerclub'); ?></a>
                                        <?php } ?>
                                        <?php $lang = get_bloginfo("language");  ?>
                                        <select name="lang-selector" id="lang-selector">
                                            <option value="<?php echo network_home_url('/'); ?>" <?php if ($lang == 'en-US') { echo 'selected'; } ?>>ENGLISH</option>
                                            <option value="<?php echo network_home_url('/es'); ?>" <?php if ($lang != 'en-US') { echo 'selected'; } ?>>ESPAÑOL</option>
                                        </select>
                                    </div>
                                    <div class="header-mobile col-xl-6 col-lg-6 col-md-3 col-sm-3 col-6 d-xl-none d-lg-none d-md-flex d-sm-flex d-flex">
                                        <div id="menu-opener" class="menu-bars">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="mobile-menu" class="header-mobile-container header-mobile-hidden col-12">
                                <div class="mobile-number">
                                    <div>
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon-call.png" alt="Call Us" class="img-fluid" />
                                    </div>
                                    <div>
                                        <h4><?php _e('Call us for assistance!', 'flowerclub'); ?></h4>
                                        <a href="tel:<?php echo $header_options['phone_number']; ?>" title="<?php _e('Call us for assistance!', 'flowerclub'); ?>"><?php echo $header_options['formatted_phone_number']; ?></a>
                                    </div>
                                </div>
                                <?php if ($header_options['subscribe_url'] != '') { ?>
                                <a href="<?php echo $header_options['subscribe_url']; ?>" class="btn btn-md btn-subscribe" target="_blank"><?php _e('Subscribe Today!', 'flowerclub'); ?></a>
                                <?php } ?>
                                <select name="lang-selector" id="lang-selector-mobile">
                                    <option value="<?php echo network_home_url('/'); ?>" <?php if ($lang == 'en-US') { echo 'selected'; } ?>>ENGLISH</option>
                                    <option value="<?php echo network_home_url('/es'); ?>" <?php if ($lang != 'en-US') { echo 'selected'; } ?>>ESPAÑOL</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
